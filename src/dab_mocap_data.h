/** \file dab_mocap_data.h
*/

#pragma once

#include <vector>
#include <map>
#include "dab_exception.h"

namespace dab
{

class MocapDataSerialize;

# pragma mark MocapData definition

class MocapData
{
public:
	friend class MocapDataSerialize;

	MocapData();
	MocapData(const std::string& pName, const std::vector<int64_t>& pDim, const std::vector<float>& pValues);
	MocapData(const MocapData& pMocapData);
	~MocapData();

	MocapData& operator=(const MocapData& pMocapData);

	const std::string& name() const;
	const std::vector<int64_t>& dim() const;

	const std::vector<float>& values() const;
	std::vector<float>& values();

	operator std::string() const;

	friend std::ostream& operator << (std::ostream& pOstream, const MocapData& pData)
	{
		std::string info = pData;
		pOstream << info;
		return pOstream;
	};

protected:
	std::string mName;
	std::vector<int64_t> mDim;
	std::vector<float> mValues;
};

# pragma mark MocapSkeleton definition

class MocapSkeleton
{
public:
	friend class MocapDataSerialize;

	MocapSkeleton();
	MocapSkeleton(const std::vector< std::string >& pJointNames, const std::vector<float>& pJointOffsets, const std::vector<int>& pJointParents);
	MocapSkeleton(const MocapSkeleton& pSkeleton);
	~MocapSkeleton();

	MocapSkeleton& operator=(const MocapSkeleton& pSkeleton);

	unsigned int jointCount() const;
	const std::vector< std::string >& jointNames() const;
	const std::vector<float>& jointOffsets() const;
	const std::vector< int >& jointParents() const;
	const std::vector< bool >& jointHasChildren() const;
	const std::vector< std::vector<int> >& jointChildren() const;

	//torch::Tensor forwardKinematics(torch::Tensor& pRotations, torch::Tensor& pRootPositions) const;

	operator std::string() const;

	friend std::ostream& operator << (std::ostream& pOstream, const MocapSkeleton& pSkeleton)
	{
		std::string info = pSkeleton;
		pOstream << info;
		return pOstream;
	};

protected:
	void computeMetadata();

	std::vector< std::string > mJointNames;
	std::vector<float> mJointOffsets;
	std::vector< int > mJointParents;
	std::vector< bool > mJointHasChildren;
	std::vector< std::vector<int> > mJointChildren;

};

# pragma mark MocapSubjectData definition

class MocapSubjectData
{
public:
	friend class MocapDataSerialize;

	MocapSubjectData();
	MocapSubjectData(const std::string& pName, const MocapSkeleton& pSkeleton);
	MocapSubjectData(const MocapSubjectData& pSubjectData);
	~MocapSubjectData();

	MocapSubjectData& operator=(const MocapSubjectData& pSubjectData);

	const std::string& name() const;
	const MocapSkeleton& skeleton() const;
	std::vector<std::string> dataNames() const;
	const std::map< std::string, MocapData >& data() const;
	const MocapData& data(const std::string& pDataName) const throw (dab::Exception);

	void addData(const MocapData& pData)  throw (dab::Exception);

	operator std::string() const;

	friend std::ostream& operator << (std::ostream& pOstream, const MocapSubjectData& pSubjectData)
	{
		std::string info = pSubjectData;
		pOstream << info;
		return pOstream;
	};

protected:
	std::string mName;
	MocapSkeleton mSkeleton;
	std::map< std::string, MocapData > mData;
};

# pragma mark MocapDataset definition

class MocapDataset
{
public:
	friend class MocapDataSerialize;

	MocapDataset();
	~MocapDataset();

	void load(const std::string& pFilePath) throw (dab::Exception);

	std::vector< std::string > subjectNames() const;

	MocapSubjectData& subjectData(const std::string& pSubjectName) throw (dab::Exception);
	const MocapSubjectData& subjectData(const std::string& pSubjectName) const throw (dab::Exception);

	operator std::string() const;

	friend std::ostream& operator << (std::ostream& pOstream, const MocapDataset& pDataset)
	{
		std::string info = pDataset;
		pOstream << info;
		return pOstream;
	};

protected:
	std::map< std::string, MocapSubjectData > mSubjects;
};

};