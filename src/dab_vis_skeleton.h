/** \file dab_vis_skeleton.h
*/

#pragma once

#include <vector>
#include "ofVectorMath.h"
#include "ofMaterial.h"
#include "dab_exception.h"
#include "dab_listener.h"

namespace dab
{

class VisShape;
class MocapSkeleton;

#pragma mark VisSkeleton definition

class VisSkeleton
{
public:
	VisSkeleton(const std::vector< std::vector<int> >& pEdgeConnectivity);
	~VisSkeleton();

	unsigned int jointCount() const;

	void setJointMaterial(const ofMaterial& pMaterial);
	void setEdgeMaterial(const ofMaterial& pMaterial);
	void setRootPosition(const glm::vec3& pPos);
	void setRootRotation(const glm::vec3& pRot);

	void update(const std::vector<glm::vec3>& pJointPositions) throw (dab::Exception);

	void draw();

protected:
	static float sJointRadius;
	static float sEdgeRadius;

	void init();

	glm::vec3 mRootPosition;
	glm::vec3 mRootRotation;

	std::vector< std::vector<int> >mEdgeConnectivity;
	std::vector< std::shared_ptr<VisShape> > mJointShapes;
	std::vector< std::shared_ptr<VisShape> > mEdgeShapes;
};

#pragma mark VisSkeletonPlayer definition

class VisSkeletonPlayer: public dab::UpdateNotifier
{
public:
	VisSkeletonPlayer(const std::vector< std::vector<int> >& pEdgeConnectivity);
	~VisSkeletonPlayer();

	std::shared_ptr<VisSkeleton> skeleton();

	void setPositionSequence(const std::vector<float>& pPositionSequence);
	void setPositionSequence(const std::vector< std::vector<glm::vec3> >& pPositionSequence);
	void setPlayRate(float pPlayRate);
	void setPlayStartTime(int pPlayStartTime);
	void setPlayStopTime(int pPlayStopTime);

	void start();
	void stop();
	void update();
	void draw();

	int playFrame() const;
	const std::vector<glm::vec3>& currentPosition() const;

protected:
	static float sFrameRate;
	static bool sLoop;

	void init(const std::vector< std::vector<int> >& pEdgeConnectivity);

	std::shared_ptr<VisSkeleton> mSkeleton;
	std::vector< std::vector<glm::vec3> > mPositionSequence;
	float mFrameRate;
	bool mPlay;
	bool mLoop;

	int mPlayFrame;
	int mPlayStartFrame;
	int mPlayStopFrame;
	double mPlayStartTime;

	float mPlayRate;
};

};