/** \file dab_mocap_data_serialize.cpp
*/

#include "dab_mocap_data_serialize.h"
#include "dab_mocap_data.h"
#include "dab_file_io.h"
#include "dab_json_helper.h"

using namespace dab;

void
MocapDataSerialize::restore(const std::string& pFileName, MocapDataset& pDataSet) throw (dab::Exception)
{
	try
	{
		std::string restoreString;
		dab::FileIO::get().read(pFileName, restoreString);

		Json::Reader reader;
		Json::Value restoreData;
		JsonHelper& jsonHelper = JsonHelper::get();

		bool parsingSuccessful = reader.parse(restoreString, restoreData);

		if (parsingSuccessful == false) throw dab::Exception("FILE ERROR: failed to parse mocap data file " + pFileName, __FILE__, __FUNCTION__, __LINE__);
		
		unsigned int subjectCount = restoreData.size();

		for (int sI = 0; sI < subjectCount; ++sI)
		{
			std::string subjectName = "S" + std::to_string(sI + 1);

			pDataSet.mSubjects[subjectName] = MocapSubjectData();
			pDataSet.mSubjects[subjectName].mName = subjectName;

			Json::Value subjectRestoreData = jsonHelper.getValue(restoreData, subjectName);
			restoreSubjectData(subjectRestoreData, pDataSet.mSubjects[subjectName]);
		}
		
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("JSON ERROR: failed to restore mocap data from file " + pFileName, __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
MocapDataSerialize::restoreSubjectData(Json::Value& pData, MocapSubjectData& pSubjectData)  throw (dab::Exception)
{
	JsonHelper& jsonHelper = JsonHelper::get();

	try
	{
		restoreSkeletonData(pData, pSubjectData.mSkeleton);

		std::vector<std::string> excludeNames = { "names", "offsets", "parents", "children" };
		const std::vector<std::string>& memberNames = pData.getMemberNames();

		for (int dI = 0; dI < pData.size(); ++dI)
		{
			const std::string memberName = memberNames[dI];
			if (std::find(excludeNames.begin(), excludeNames.end(), memberName) != excludeNames.end()) continue;

			pSubjectData.mData[memberName] = MocapData();
			pSubjectData.mData[memberName].mName = memberName;

			restoreMocapData(pData[memberName], pSubjectData.mData[memberName]);
		}
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("JSON ERROR: failed to restore subject", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
MocapDataSerialize::restoreSkeletonData(Json::Value& pData, MocapSkeleton& pSubjectData) throw (dab::Exception)
{
	JsonHelper& jsonHelper = JsonHelper::get();

	try
	{
		std::vector<std::string> jointNames;
		std::vector<float> jointOffsets;
		std::vector<int> jointParents;

		jsonHelper.getStrings(pData, "names", jointNames);
		jsonHelper.getInts(pData, "parents", jointParents);

		unsigned int jointCount = jointNames.size();

		Json::Value jointOffsetsRestoreData = jsonHelper.getValue(pData, "offsets");
		for (int jI = 0; jI < jointOffsetsRestoreData.size(); ++jI)
		{
			std::vector<float> offsets;
			Json::Value jointOffsetRestoreData = jsonHelper.getValue(jointOffsetsRestoreData, jI);

			for (int d = 0; d < jointOffsetRestoreData.size(); ++d)
			{
				jointOffsets.push_back(jsonHelper.getFloat(jointOffsetRestoreData, d));
			}
		}

		pSubjectData = MocapSkeleton(jointNames, jointOffsets, jointParents);
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("JSON ERROR: failed to restore skeleton", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
MocapDataSerialize::restoreMocapData(Json::Value& pData, MocapData& pMocapData) throw (dab::Exception)
{
	JsonHelper& jsonHelper = JsonHelper::get();

	try
	{
		Json::Value* _data = &pData;
		std::vector<int64_t> dim;

		int valueCount = 1;
		while (_data->size() >= 1)
		{
			dim.push_back(_data->size());
			valueCount *= _data->size();
			_data = &((*_data)[0]);
		}

		int dataDim = dim.size();
		std::vector<unsigned int> dataIndices(dataDim, 0);
		pMocapData.mDim = dim;
		pMocapData.mValues.resize(valueCount);

		int valueIndex = 0;
		while (valueIndex < valueCount)
		{
			_data = &pData;

			for (int dI = 0; dI < dataDim; ++dI)
			{
				_data = (&(*_data)[dataIndices[dI]]);
			}

			pMocapData.mValues[valueIndex++] = _data->asFloat();

			bool indexIncr = true;
			for (int dI = dataDim - 1; dI >= 0; --dI)
			{
				if (indexIncr == true)
				{
					dataIndices[dI] += 1;
					indexIncr = false;

					if (dataIndices[dI] >= dim[dI])
					{
						dataIndices[dI] = 0;
						indexIncr = true;
					}
				}
			}
		}
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("JSON ERROR: failed to restore mocap data", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}
