#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "dab_osc_receiver.h"
#include "dab_osc_sender.h"
#include "dab_listener.h"
#include <memory>
#include <thread>
#include <future>
#include "dab_mocap_data.h"
#include "dab_mocap_player.h"
#include "dab_vis_shape.h"
#include "dab_vis_skeleton.h"

class ofApp : public ofBaseApp, public dab::OscListener, public dab::UpdateListener
{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void mouseScrolled(int x, int y, float scrollX, float scrollY);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void notify(std::shared_ptr<dab::OscMessage> pMessage);
		void updateOsc();
		void updateOsc(std::shared_ptr<dab::OscMessage> pMessage);

		void notifyUpdate();

	protected:
		std::shared_ptr<ofApp> mSelf;

		// config
		void loadConfig(const std::string& pFileName) throw (dab::Exception);

		// mocap
		void setupMocap();
		void loadMocapFile(const std::string& pFilePath) throw (dab::Exception);
		void updateMocap();
		void updateMocap(const std::vector<float>& pMocapSample) throw (dab::Exception);
		std::string mMocapFileName;
		dab::MocapDataset mMocapDataset;
		std::shared_ptr<dab::MocapPlayer> mMocapPlayer;
		int mMocapWindowLength;
		int mMocapWindowOffset;
		int mMocapJointCount;
		int mMocapJointDim;
		int mMocapLatentDim;
		int mMocapReadFrame;


		// OSC Communication
		void setupOsc() throw (dab::Exception);
		void updateOscSender() throw (dab::Exception);
		std::string mOscSendAddress;
		int mOscSendPort;
		std::mutex mOscLock;
		dab::OscReceiver* mOscReceiver;
		dab::OscSender* mOscSender;
		unsigned int mMaxOscMessageQueueLength = 1000;
		std::deque< std::shared_ptr<dab::OscMessage> > mOscMessageQueue;
		void updateMocap(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception);

		// visual
		void setupGraphics() throw (dab::Exception);
		void updateGraphics() throw (dab::Exception);
		void displayGraphics() throw (dab::Exception);
		std::shared_ptr<ofCamera> mCamera;
		std::shared_ptr<ofLight> mPointLight;
		std::shared_ptr<dab::VisSkeleton> mVisSkeleton;

		// camera
		bool mMouseDragActive = false;
		glm::vec2 mMouseDragStartPos;
		// captury settings
		//float mCameraVerticalOffset = 80.0;
		//float mCameraAzumith = -PI/2.0;
		//float mCameraElevation = 0.001;
		//float mCameraDistance = 800.0;
		// qualisys settings
		//float mCameraVerticalOffset = 80.0;
		//float mCameraAzumith = PI; // for Muriel Mocap: PI
		//float mCameraElevation = PI / 2.0; // for Muriel Mocap: PI / 2.0
		//float mCameraDistance = 400.0; // for Muriel Mocap: 1000.0

		// qualisys motionbuilder standard
		float mCameraVerticalOffset = 160.0;
		float mCameraAzumith = -PI / 2.0; // for Muriel Mocap: PI
		float mCameraElevation = 0.001; // for Muriel Mocap: PI / 2.0
		float mCameraDistance = 600.0; // for Muriel Mocap: 1000.0

		float mMocapFPS = 50;
		int mSeqIndex = 0;

		void updateCamera();

		// Gui
		ofxImGui::Gui mGui;
		
};
