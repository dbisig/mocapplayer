#include "ofApp.h"
#include "ofMathConstants.h"
#include <memory>
#include <iostream>
#include <cmath>
#include <typeinfo>
#include <type_traits>
#include "dab_exception.h"
#include "dab_file_io.h"
#include "dab_json_helper.h"
#include "dab_ringbuffer.h"
#include "dab_vis_skeleton.h"


//--------------------------------------------------------------
void ofApp::setup()
{
	try
	{
		mSelf = std::shared_ptr<ofApp>(this);

		loadConfig(ofToDataPath("config.json"));
		setupMocap();
		loadMocapFile(mMocapFileName);

		setupOsc();

		setupGraphics();

		mGui.setup();
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << "\n";
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

//--------------------------------------------------------------
void ofApp::update()
{	
	try
	{
		if(mMocapPlayer != nullptr) mMocapPlayer->update();
		//updateOsc();
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
	if (button == 2)
	{
		if (mMouseDragActive == false)
		{
			mMouseDragActive = true;
			mMouseDragStartPos = glm::vec2(x, y);
		}
		else
		{
			glm::vec2 mouseDragVec = glm::vec2(x, y) - mMouseDragStartPos;
			mCameraAzumith += mouseDragVec.y / static_cast<float>(ofGetWindowHeight()) * PI * 0.1;
			mCameraElevation += mouseDragVec.x / static_cast<float>(ofGetWindowWidth()) * PI * 0.1;

			//std::cout << "drag x " << mouseDragVec.x << " y " << mouseDragVec.y << " az " << mCameraAzumith << " el " << mCameraElevation << "\n";

			updateCamera();
		}
	}
	else
	{
		mMouseDragActive = false;
	}
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY)
{
	//std::cout << "scrollX " << scrollX << " scrollY " << scrollY << "\n";

	mCameraDistance += scrollY * 10.0;

	updateCamera();
}


//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------

void 
ofApp::loadConfig(const std::string& pFileName) throw (dab::Exception)
{
	try
	{
		std::string restoreString;
		dab::FileIO::get().read(pFileName, restoreString);

		Json::Reader reader;
		Json::Value restoreData;
		dab::JsonHelper& jsonHelper = dab::JsonHelper::get();

		bool parsingSuccessful = reader.parse(restoreString, restoreData);

		if (parsingSuccessful == false) throw dab::Exception("FILE ERROR: failed to parse config data file " + pFileName, __FILE__, __FUNCTION__, __LINE__);

		mMocapFileName = jsonHelper.getString(restoreData, "mocapFileName");
		mOscSendAddress = jsonHelper.getString(restoreData, "oscSendAddress");
		mOscSendPort = jsonHelper.getInt(restoreData, "oscSendPort");
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("JSON ERROR: failed to restore config from file " + pFileName, __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
ofApp::setupMocap()
{}

void 
ofApp::loadMocapFile(const std::string& pFilePath) throw (dab::Exception)
{
	try
	{
		mMocapDataset.load(pFilePath);

		// create visual skeleton player
		std::string subjectName = mMocapDataset.subjectNames()[0];
		const dab::MocapSubjectData& subjectData = mMocapDataset.subjectData(subjectName);
		const dab::MocapSkeleton& skeleton = subjectData.skeleton();

		const dab::MocapData& mocapPosData = mMocapDataset.subjectData(subjectName).data("pos_world");
		const std::vector<int64_t>& mocapPosDim = mocapPosData.dim();
		const std::vector<float>& mocapPosValues = mocapPosData.values();

		const dab::MocapData& mocapRotData = mMocapDataset.subjectData(subjectName).data("rot_world");
		const std::vector<int64_t>& mocapRotDim = mocapRotData.dim();
		const std::vector<float>& mocapRotValues = mocapRotData.values();

		const dab::MocapData& mocapLocalRotData = mMocapDataset.subjectData(subjectName).data("rot_local");
		const std::vector<int64_t>& mocapLocalRotDim = mocapLocalRotData.dim();
		const std::vector<float>& mocapLocalRotValues = mocapLocalRotData.values();

		mMocapPlayer = std::make_shared<dab::MocapPlayer>();
		mMocapPlayer->setPositionSequence(mocapPosDim, mocapPosValues);
		mMocapPlayer->setRotationSequence(mocapRotDim, mocapRotValues);
		mMocapPlayer->setLocalRotationSequence(mocapLocalRotDim, mocapLocalRotValues);
		mMocapPlayer->addListener(mSelf);
		mMocapPlayer->start();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("MOCAP ERROR: failed to load mocap data", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void
ofApp::updateMocap()
{
	////std::cout << "ofApp::updateMocap()\n";

	//int playFrame = mVisSkeletonPlayer->playFrame();

	////std::cout << "playFrame " << playFrame << "\n";

	//if (playFrame == mMocapWindowOffset - 1) // player reached end of playable sequence
	//{
	//	mVisSkeletonPlayer->setPositionSequence(mPositionSequenceUpdateVector);

	//	if (mMLThreadDone == true)
	//	{
	//		mMLThreadHandle = std::async(std::launch::async, &ofApp::updateML, this);
	//	}
	//}
}

void
ofApp::updateMocap(const std::vector<float>& pMocapSample) throw (dab::Exception)
{
	//std::array<float, 4> imuOrient = { pMocapSample[6], pMocapSample[7], pMocapSample[8], pMocapSample[9] };

	//mMocapLiveRingBuffer->update(imuOrient);

	//for (int sI = 0, vI = 0; sI < mMocapWindowLength; ++sI)
	//{
	//	const std::array<float, 4>& mocapSample = (*mMocapLiveRingBuffer)[mMocapWindowLength - (sI + 1)];

	//	for (int dI = 0; dI < 4; ++dI, ++vI)
	//	{
	//		mMocapLiveValues[vI] = mocapSample[dI];
	//	}
	//}
}

void
ofApp::setupOsc() throw (dab::Exception)
{
	try
	{
		mOscReceiver = new dab::OscReceiver("MocapReceiver", 9002);
		mOscReceiver->registerOscListener(std::weak_ptr<ofApp>(mSelf));
		mOscReceiver->start();

		mOscSender = new dab::OscSender("MocapSender", mOscSendAddress, mOscSendPort);
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void
ofApp::notify(std::shared_ptr<dab::OscMessage> pMessage)
{
	mOscLock.lock();

	mOscMessageQueue.push_back(pMessage);
	if (mOscMessageQueue.size() > mMaxOscMessageQueueLength) mOscMessageQueue.pop_front();

	mOscLock.unlock();
}

void
ofApp::updateOsc()
{
	mOscLock.lock();

	while (mOscMessageQueue.size() > 0)
	{
		std::shared_ptr< dab::OscMessage > oscMessage = mOscMessageQueue[0];

		updateOsc(oscMessage);

		mOscMessageQueue.pop_front();
	}

	mOscLock.unlock();
}

void
ofApp::updateOsc(std::shared_ptr<dab::OscMessage> pMessage)
{
	try
	{
		std::string address = pMessage->address();

		//std::cout << "address " << address << "\n";

		const std::vector<dab::_OscArg*>& arguments = pMessage->arguments();

		if (address.compare("/imu/1") == 0) updateMocap(arguments);
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void 
ofApp::updateOscSender() throw (dab::Exception)
{
	try
	{
		// send joint positions
		{
			const std::vector<glm::vec3>& currentPos = mMocapPlayer->currentPosition();

			int posCount = currentPos.size();

			std::string messageAddress = "/mocap/joint/pos";
			std::shared_ptr<dab::OscMessage> message(new dab::OscMessage(messageAddress));

			for (int pI = 0; pI < posCount; ++pI)
			{
				message->add(currentPos[pI].x);
				message->add(currentPos[pI].y);
				message->add(currentPos[pI].z);
			}

			mOscSender->send(message);
		}

		// send joint rotations
		{
			const std::vector<glm::quat>& currentRot = mMocapPlayer->currentRotation();

			int rotCount = currentRot.size();

			std::string messageAddress = "/mocap/joint/rot";
			std::shared_ptr<dab::OscMessage> message(new dab::OscMessage(messageAddress));

			for (int pI = 0; pI < rotCount; ++pI)
			{
				/*
				// send euler
				glm::vec3 euler = glm::eulerAngles(currentRot[pI]);

				message->add(euler[0]);
				message->add(euler[1]);
				message->add(euler[2]);
				*/

				// send quaternion
				message->add(currentRot[pI][0]);
				message->add(currentRot[pI][1]);
				message->add(currentRot[pI][2]);
				message->add(currentRot[pI][3]);
			}

			mOscSender->send(message);
		}

		// 	send local joint rotations
		{
			const std::vector<glm::quat>& currentLocalRot = mMocapPlayer->currentLocalRotation();

			int rotCount = currentLocalRot.size();

			std::string messageAddress = "/mocap/joint/locrot";
			std::shared_ptr<dab::OscMessage> message(new dab::OscMessage(messageAddress));

			for (int pI = 0; pI < rotCount; ++pI)
			{
				/*
				// send euler
				glm::vec3 euler = glm::eulerAngles(currentRot[pI]);

				message->add(euler[0]);
				message->add(euler[1]);
				message->add(euler[2]);
				*/

				// send quaternion
				message->add(currentLocalRot[pI][0]);
				message->add(currentLocalRot[pI][1]);
				message->add(currentLocalRot[pI][2]);
				message->add(currentLocalRot[pI][3]);
			}

			mOscSender->send(message);
		}
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void 
ofApp::notifyUpdate()
{
	try
	{
		updateGraphics();
		updateOscSender();
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void
ofApp::updateMocap(const std::vector<dab::_OscArg*>& pArgs) throw (dab::Exception)
{
	try
	{
		int argCount = pArgs.size();
		std::vector<float> mocapSample(argCount);
		for (int aI = 0; aI < argCount; ++aI)
		{
			float value = *pArgs[aI];
			mocapSample[aI] = value;
		}

		//std::cout << "mocapSample " << mocapSample << "\n";

		updateMocap(mocapSample);
	}
	catch (dab::Exception& e)
	{
		throw e;
	}
}

void 
ofApp::setupGraphics() throw (dab::Exception)
{
	try
	{
		ofDisableArbTex(); // important: otherwise all textures will be of type RECT2D instead of 2D
		ofDisableLighting();
		ofEnableDepthTest();
		ofSetVerticalSync(true);
		ofBackground(0);

		// create camera
		mCamera = std::shared_ptr<ofCamera>(new ofCamera());
		updateCamera();

		// create light source
		ofEnableLighting();
		ofSetSmoothLighting(true);
		mPointLight = std::make_shared<ofLight>();
		mPointLight->setPointLight();

		// create visual skeleton
		std::string subjectName = mMocapDataset.subjectNames()[0];
		const dab::MocapSubjectData& subjectData = mMocapDataset.subjectData(subjectName);
		const dab::MocapSkeleton& skeleton = subjectData.skeleton();
		mVisSkeleton = std::make_shared<dab::VisSkeleton>(skeleton.jointChildren());

		ofMaterial jointMaterial;
		ofMaterial edgeMaterial;

		jointMaterial.setAmbientColor(ofColor::fromHsb(0.2 * 255.0, 0.4 * 255.0, 0.5 * 255.0));
		jointMaterial.setDiffuseColor(ofColor::fromHsb(0.2 * 255.0, 0.4 * 255.0, 0.5 * 255.0));
		jointMaterial.setSpecularColor(ofColor::fromHsb(0.2 * 255.0, 0.4 * 255.0, 0.5 * 255.0));
		jointMaterial.setShininess(100.0);

		edgeMaterial.setAmbientColor(ofColor::fromHsb(0.2 * 255.0, 0.2 * 255.0, 0.5 * 255.0));
		edgeMaterial.setDiffuseColor(ofColor::fromHsb(0.2 * 255.0, 0.2 * 255.0, 0.5 * 255.0));
		edgeMaterial.setSpecularColor(ofColor::fromHsb(0.2 * 255.0, 0.2 * 255.0, 0.5 * 255.0));
		edgeMaterial.setShininess(100.0);

		mVisSkeleton->setJointMaterial(jointMaterial);
		mVisSkeleton->setEdgeMaterial(edgeMaterial);

		// configure light settings
		mPointLight->setPosition(-100.0, 100.0, 1000);
		mPointLight->setAmbientColor(ofColor::fromHsb(0.0 * 255.0, 0.0 * 255.0, 0.13871 * 255.0));
		mPointLight->setAttenuation(0.745602, 0.0, 0.0);
		mPointLight->setDiffuseColor(ofColor::fromHsb(0.0 * 255.0, 0.0 * 255.0, 1.0 * 255.0));
		mPointLight->setSpecularColor(ofColor::fromHsb(0.0 * 255.0, 0.0 * 255.0, 1.0 * 255.0));
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to setup graphics", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void
ofApp::updateGraphics() throw (dab::Exception)
{
	const std::vector<glm::vec3>& currentPos = mMocapPlayer->currentPosition();
	mVisSkeleton->update(currentPos);

	//mVisSkeleton->setRootRotation(glm::vec3(mCameraAzumith, mCameraElevation, 0.0));
}




//--------------------------------------------------------------
void ofApp::draw()
{
	try
	{
		displayGraphics();

		mGui.begin();

		bool isPlaying = mMocapPlayer->isPlaying();
		float playRate = mMocapPlayer->playRate();
		float playStartTime = mMocapPlayer->playStartTime();
		float playStopTime = mMocapPlayer->playStopTime();
		bool loop = mMocapPlayer->loop();
		float playTime = mMocapPlayer->playTime();

		if (ImGui::Checkbox("isPlaying", &isPlaying))
		{
			if (isPlaying == true) mMocapPlayer->start();
			else mMocapPlayer->stop();
		}

		ImGui::InputFloat("playTime", &playTime);
		
		if (ImGui::InputFloat("playRate", &playRate)) mMocapPlayer->setPlayRate(playRate);
		if (ImGui::InputFloat("startTime", &playStartTime)) mMocapPlayer->setPlayStartTime(playStartTime);
		if (ImGui::InputFloat("endTime", &playStopTime)) mMocapPlayer->setPlayStopTime(playStopTime);
		if (ImGui::Checkbox("loop", &loop)) mMocapPlayer->setLoop(loop);

		if (ImGui::InputFloat("camVOffset", &mCameraVerticalOffset)) updateCamera();
		if (ImGui::InputFloat("camAzi", &mCameraAzumith)) updateCamera();
		if (ImGui::InputFloat("camEle", &mCameraElevation)) updateCamera();
		if (ImGui::InputFloat("camDist", &mCameraDistance)) updateCamera();


		mGui.end();

		//const std::vector<glm::vec3>& jointPos = mMocapPlayer->currentPosition();
		//const glm::vec3 leftHandJointPos = jointPos[11];
		//std::string debugString = std::string("LeftHandPos Z " + std::to_string(leftHandJointPos.z));

		//ofDrawBitmapString(debugString, 100, 100);
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

void 
ofApp::displayGraphics() throw (dab::Exception)
{
	ofSetColor(255.0, 255.0, 255.0);

	ofEnableLighting();
	mPointLight->enable();

	mCamera->begin();
	mVisSkeleton->draw();
	mCamera->end();
}

void 
ofApp::updateCamera()
{
	glm::vec3 cameraPos(0.0, 0.0, 0.0);
	cameraPos.x = cos(mCameraAzumith) * sin(mCameraElevation) * mCameraDistance;
	cameraPos.y = sin(mCameraAzumith) * sin(mCameraElevation) * mCameraDistance;
	cameraPos.z = cos(mCameraElevation) * mCameraDistance;

	//cameraPos.y += mCameraVerticalOffset;
	cameraPos.z += mCameraDistance;

	mCamera->setPosition(cameraPos);
	mCamera->lookAt(ofVec3f(0.0, mCameraVerticalOffset, 0.0), ofVec3f(0.0, 0.0, 1.0));
	//mCamera->lookAt(ofVec3f(0.0, 0.0, mCameraVerticalOffset), ofVec3f(0.0, 0.0, 1.0));
	mCamera->setFov(20.0);

	std::cout << "mCameraAzumith " << mCameraAzumith << " mCameraElevation " << mCameraElevation << " mCameraDistance " << mCameraDistance << "\n";
}
