#include "dab_mocap_player.h"
#include "ofApp.h"

#pragma mark MocapPlayer implementation

using namespace dab;

float MocapPlayer::sFrameRate = 50.0;
bool MocapPlayer::sLoop = true;

MocapPlayer::MocapPlayer()
	: mFrameRate(sFrameRate)
	, mLoop(sLoop)
	, mPlay(false)
	, mPlayStartFrame(0)
	, mPlayRate(sFrameRate)
{}

MocapPlayer::~MocapPlayer()
{}

void
MocapPlayer::setPositionSequence(const std::vector<int64_t>& pPosDim, const std::vector<float>& pPosValues)
{
	int frameCount = pPosDim[0];
	int jointCount = pPosDim[1];
	int jointDim = pPosDim[2];

	mPositionSequence = std::vector< std::vector<glm::vec3> >(frameCount, std::vector<glm::vec3>(jointCount));

	for (int fI = 0, dI = 0; fI < frameCount; fI++)
	{
		for (int jI = 0; jI < jointCount; ++jI, dI += jointDim)
		{
			//std::cout << "fI " << fI << " jI " << jI << "\n";

			mPositionSequence[fI][jI] = glm::vec3(pPosValues[dI], pPosValues[dI + 1], pPosValues[dI + 2]);
		}
	}

	mPlayStopFrame = mPositionSequence.size();
	mPlayStartTime = 0.0;
	mPlayStopTime = static_cast<float>(mPlayStopFrame) / mFrameRate;
}

void
MocapPlayer::setPositionSequence(const std::vector< std::vector<glm::vec3> >& pPositionSequence)
{
	mPositionSequence = pPositionSequence;
	mPlayStopFrame = mPositionSequence.size();
	mPlayStartTime = 0.0;
	mPlayStopTime = static_cast<float>(mPlayStopFrame) / mFrameRate;
}

void 
MocapPlayer::setRotationSequence(const std::vector<int64_t>& pRotDim, const std::vector<float>& pRotValues)
{
	int frameCount = pRotDim[0];
	int jointCount = pRotDim[1];
	int jointDim = pRotDim[2];

	mRotationSequence = std::vector< std::vector<glm::quat> >(frameCount, std::vector<glm::quat>(jointCount));

	for (int fI = 0, dI = 0; fI < frameCount; fI++)
	{
		for (int jI = 0; jI < jointCount; ++jI, dI += jointDim)
		{
			//std::cout << "fI " << fI << " jI " << jI << "\n";

			mRotationSequence[fI][jI] = glm::quat(pRotValues[dI], pRotValues[dI + 1], pRotValues[dI + 2], pRotValues[dI + 3]);
		}
	}

	mPlayStopFrame = mRotationSequence.size();
	mPlayStartTime = 0.0;
	mPlayStopTime = static_cast<float>(mPlayStopFrame) / mFrameRate;
}

void 
MocapPlayer::setLocalRotationSequence(const std::vector<int64_t>& pRotDim, const std::vector<float>& pLocalRotValues)
{
	int frameCount = pRotDim[0];
	int jointCount = pRotDim[1];
	int jointDim = pRotDim[2];

	mLocalRotationSequence = std::vector< std::vector<glm::quat> >(frameCount, std::vector<glm::quat>(jointCount));

	for (int fI = 0, dI = 0; fI < frameCount; fI++)
	{
		for (int jI = 0; jI < jointCount; ++jI, dI += jointDim)
		{
			//std::cout << "fI " << fI << " jI " << jI << "\n";

			mLocalRotationSequence[fI][jI] = glm::quat(pLocalRotValues[dI], pLocalRotValues[dI + 1], pLocalRotValues[dI + 2], pLocalRotValues[dI + 3]);
		}
	}

	mPlayStopFrame = mLocalRotationSequence.size();
	mPlayStartTime = 0.0;
	mPlayStopTime = static_cast<float>(mPlayStopFrame) / mFrameRate;
}

bool 
MocapPlayer::isPlaying() const
{
	return mPlay;
}

float 
MocapPlayer::playRate() const
{
	return mPlayRate;
}

float 
MocapPlayer::playStartTime() const
{
	return mPlayStartTime;
}

float 
MocapPlayer::playStopTime() const
{
	return mPlayStopTime;
}

bool 
MocapPlayer::loop() const
{
	return mLoop;
}

float 
MocapPlayer::playTime() const
{
	return static_cast<float>(mPlayFrame) / mFrameRate;
}

void
MocapPlayer::setPlayRate(float pFrameRate)
{
	mPlayRate = pFrameRate;
}

void
MocapPlayer::setPlayStartTime(float pPlayStartTime)
{
	int _playStartFrame = pPlayStartTime * mPlayRate;

	if (_playStartFrame < 0) _playStartFrame = 0;
	if (_playStartFrame > mPlayStopFrame) _playStartFrame = mPlayStopFrame;

	mPlayStartFrame = _playStartFrame;
	mPlayStartTime = static_cast<float>(mPlayStartFrame) / mFrameRate;
}

void
MocapPlayer::setPlayStopTime(float pPlayStopTime)
{
	int _playStopFrame = pPlayStopTime * mPlayRate;

	if (_playStopFrame < mPlayStartFrame) _playStopFrame = mPlayStartFrame;
	if (_playStopFrame >= mPositionSequence.size()) _playStopFrame = mPositionSequence.size() - 1;

	mPlayStopFrame = _playStopFrame;
	mPlayStopTime = static_cast<float>(mPlayStopFrame) / mFrameRate;
}

void 
MocapPlayer::setLoop(bool pLoop)
{
	mLoop = pLoop;
}

void
MocapPlayer::start()
{
	mStartTime = static_cast<double>(ofGetElapsedTimeMillis()) / 1000.0;
	mPlayFrame = mPlayStartFrame;
	mPlay = true;
}

void
MocapPlayer::stop()
{
	mPlay = false;
}

const std::vector<glm::vec3>&
MocapPlayer::currentPosition() const
{
	return mPositionSequence[mPlayFrame];
}

const std::vector<glm::quat>&
MocapPlayer::currentRotation() const
{
	return mRotationSequence[mPlayFrame];
}

const std::vector<glm::quat>&
MocapPlayer::currentLocalRotation() const
{
	return mLocalRotationSequence[mPlayFrame];
}

void
MocapPlayer::update()
{
	if (mPlay == false) return;

	double currentTime = static_cast<double>(ofGetElapsedTimeMillis()) / 1000.0;
	double elapsedTime = currentTime - mStartTime;
	int relPlayFrame = mPlayFrame - mPlayStartFrame;
	//double nextFrameTime = static_cast<float>(relPlayFrame) / mFrameRate;
	double nextFrameTime = static_cast<float>(relPlayFrame) / mPlayRate;
	if (elapsedTime > nextFrameTime)
	{
		mPlayFrame++;

		if (mPlayFrame >= mPlayStopFrame)
		{
			if (mLoop == true) start();
			else stop();
		}

		// update listeners
		int listenerCount = mListeners.size();

		for (int lI = listenerCount - 1; lI >= 0; --lI)
		{
			std::shared_ptr<dab::UpdateListener> listener = mListeners[lI].lock();

			//std::cout << "lI " << lI << " ptr " << listener << "\n";

			if (listener != nullptr)
			{
				mListeners[lI].lock()->notifyUpdate();
			}
		}
	}
}

int
MocapPlayer::playFrame() const
{
	return mPlayFrame;
}
