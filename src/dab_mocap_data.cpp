/** \file dab_mocap_data.cpp
*/

#include "dab_mocap_data.h"
#include "dab_mocap_data_serialize.h"
#include "ofVectorMath.h"

using namespace dab;

# pragma mark MocapData implementation

MocapData::MocapData()
	: mName("")
{}

MocapData::MocapData(const std::string& pName, const std::vector<int64_t>& pDim, const std::vector<float>& pValues)
	: mName(pName)
	, mDim(pDim)
	, mValues(pValues)
{}

MocapData::MocapData(const MocapData& pMocapData)
	: mName(pMocapData.mName)
	, mDim(pMocapData.mDim)
	, mValues(pMocapData.mValues)
{}

MocapData::~MocapData()
{}

MocapData& 
MocapData::operator=(const MocapData& pMocapData)
{
	mName = pMocapData.mName;
	mDim = pMocapData.mDim;
	mValues = pMocapData.mValues;

	return *this;
}

const std::string& 
MocapData::name() const
{
	return mName;
}
	
const std::vector<int64_t>&
MocapData::dim() const
{
	return mDim;
}

const std::vector<float>&
MocapData::values() const
{
	return mValues;
}

std::vector<float>&
MocapData::values()
{
	return mValues;
}

MocapData::operator std::string() const
{
	std::stringstream ss;

	ss << "MocapData:\n";
	ss << "name: " << mName << "\n";
	ss << "dim: [";
	for (int vI = 0; vI < mDim.size(); ++vI) ss << " " << mDim[vI];
	ss << " ]\n";
	//ss << "values: [";
	//for (int vI = 0; vI < mValues.size(); ++vI) ss << " " << mValues[vI];
	//ss << " ]\n";

	return ss.str();
}

# pragma mark MocapSkeleton implementation

MocapSkeleton::MocapSkeleton()
{}

MocapSkeleton::MocapSkeleton(const std::vector<std::string>& pJointNames, const std::vector<float>& pJointOffsets, const std::vector<int>& pJointParents)
	: mJointNames(pJointNames)
	, mJointOffsets(pJointOffsets)
	, mJointParents(pJointParents)
{
	computeMetadata();
}

MocapSkeleton::MocapSkeleton(const MocapSkeleton& pSkeleton)
	: mJointNames(pSkeleton.mJointNames)
	, mJointOffsets(pSkeleton.mJointOffsets)
	, mJointParents(pSkeleton.mJointParents)
	, mJointHasChildren(pSkeleton.mJointHasChildren)
	, mJointChildren(pSkeleton.mJointChildren)
{}

MocapSkeleton::~MocapSkeleton()
{}

MocapSkeleton& 
MocapSkeleton::operator=(const MocapSkeleton& pSkeleton)
{
	mJointNames = pSkeleton.mJointNames;
	mJointOffsets = pSkeleton.mJointOffsets;
	mJointParents = pSkeleton.mJointParents;
	mJointHasChildren = pSkeleton.mJointHasChildren;
	mJointChildren = pSkeleton.mJointChildren;

	return *this;
}

unsigned int 
MocapSkeleton::jointCount() const
{
	return mJointNames.size();
}

const std::vector< std::string >& 
MocapSkeleton::jointNames() const
{
	return mJointNames;
}

const std::vector<float>&
MocapSkeleton::jointOffsets() const
{
	return mJointOffsets;
}

const std::vector< int >& 
MocapSkeleton::jointParents() const
{
	return mJointParents;
}

const std::vector< bool >& 
MocapSkeleton::jointHasChildren() const
{
	return mJointHasChildren;
}
	
const std::vector< std::vector<int> >& 
MocapSkeleton::jointChildren() const
{
	return mJointChildren;
}

//torch::Tensor 
//MocapSkeleton::forwardKinematics(torch::Tensor& pRotations, torch::Tensor& pRootPositions) const
//{
//	/*
//	Perform forward kinematics using the given trajectory and local rotations.
//	Arguments(where N = batch size, L = sequence length, J = number of joints) :
//	--rotations : (N, L, J, 4) tensor of unit quaternions describing the local rotations of each joint.
//	--root_positions : (N, L, 3) tensor describing the root joint positions.
//	*/
//
//	TorchQuat& torchquat = TorchQuat::get();
//
//	assert(pRotations.dim() == 4);
//	assert(pRotations.size(pRotations.dim() - 1) == 4);
//
//	std::vector< torch::Tensor > positionsWorld;
//	std::vector< torch::Tensor > rotationsWorld;
//
//	torch::Tensor expandedOffsets = mJointOffsets.expand({ pRotations.size(0), pRotations.size(1), mJointOffsets.size(0), mJointOffsets.size(1) });
//
//	/*
//	Parallelize along the batch and time dimensions
//	loop over joints
//	*/
//
//	int jointCount = mJointOffsets.size(0);
//	for (int jI = 0; jI < jointCount; ++jI)
//	{
//		if (mJointParents[jI] == -1)
//		{
//			positionsWorld.push_back(pRootPositions);
//			rotationsWorld.push_back(pRotations.index({Slice(None), Slice(None), 0}));
//		}
//		else
//		{
//			torch::Tensor pw = torchquat.qrot(rotationsWorld[mJointParents[jI]], expandedOffsets.index({ Slice(None), Slice(None), jI }));
//			pw += positionsWorld[mJointParents[jI]];
//
//			positionsWorld.push_back(pw);
//
//			if (mJointHasChildren[jI] == true)
//			{
//				torch::Tensor rw = torchquat.qmul(rotationsWorld[mJointParents[jI]], pRotations.index({Slice(None), Slice(None), jI}));
//				rotationsWorld.push_back(rw);
//			}
//			else
//			{
//				// This joint is a terminal node -> it would be useless to compute the transformation
//				rotationsWorld.push_back(torch::empty({1}));
//			}
//		}
//	}
//
//	return torch::stack(positionsWorld, 3).permute({ 0, 1, 3, 2 });
//}

MocapSkeleton::operator std::string() const
{
	std::stringstream ss;

	int joint_count = mJointNames.size();

	ss << "MocapSkeleton:\n";
	ss << "joint names: [";
	for (int jI = 0; jI < joint_count; ++jI) ss << " " << mJointNames[jI];
	ss << " ]\n";

	ss << "joint offsets: [";
	for (int jI = 0; jI < joint_count; ++jI)
	{
		ss << " [ ";
		for (int dI = 0; dI < 3; ++dI)
		{
			ss << " " << mJointOffsets[jI*3+dI];
		}
		ss << " ] ";
	}
	ss << " ]\n";

	ss << "joint parents: [";
	for (int jI = 0; jI < joint_count; ++jI) ss << " " << mJointParents[jI];
	ss << " ]\n";

	ss << "joint hasChildren: [";
	for (int jI = 0; jI < joint_count; ++jI) ss << " " << mJointHasChildren[jI];
	ss << " ]\n";

	ss << "joint children: [";
	for (int jI = 0; jI < joint_count; ++jI)
	{
		ss << "[";

		int childCount = mJointChildren[jI].size();

		for (int cI = 0; cI < childCount; ++cI)
		{
			ss << " " << mJointChildren[jI][cI];
		}

		ss << " ]";
	}
	ss << "]\n";

	return ss.str();
}

void 
MocapSkeleton::computeMetadata()
{
	int joint_count = mJointNames.size();

	mJointHasChildren.resize(joint_count, false);
	mJointChildren.resize(joint_count);

	for (int jI = 0; jI < joint_count; ++jI)
	{
		if (mJointParents[jI] != -1)
		{
			mJointHasChildren[jI] = true;
			mJointChildren[mJointParents[jI]].push_back(jI);
		}
	}

	mJointChildren.resize(joint_count);
}

# pragma mark MocapSubjectData implementation

MocapSubjectData::MocapSubjectData()
{}

MocapSubjectData::MocapSubjectData(const std::string& pName, const MocapSkeleton& pSkeleton)
	: mName(pName)
	, mSkeleton(pSkeleton)
{}

MocapSubjectData::MocapSubjectData(const MocapSubjectData& pSubjectData)
	: mName(pSubjectData.mName)
	, mSkeleton(pSubjectData.mSkeleton)
	, mData(pSubjectData.mData)
{}

MocapSubjectData::~MocapSubjectData()
{}

MocapSubjectData& 
MocapSubjectData::operator=(const MocapSubjectData& pSubjectData)
{
	mName = pSubjectData.mName;
	mSkeleton = pSubjectData.mSkeleton;
	mData = pSubjectData.mData;

	return *this;
}

const std::string& 
MocapSubjectData::name() const
{
	return mName;
}

const MocapSkeleton& 
MocapSubjectData::skeleton() const
{
	return mSkeleton;
}

std::vector<std::string> 
MocapSubjectData::dataNames() const
{
	std::vector<std::string> _dataNames;
	for (const auto& item : mData) _dataNames.push_back(item.first);
	return _dataNames;
}

const std::map< std::string, MocapData >& 
MocapSubjectData::data() const
{
	return mData;
}

const MocapData& 
MocapSubjectData::data(const std::string& pDataName) const throw (dab::Exception)
{
	if (mData.find(pDataName) == mData.end()) throw dab::Exception("MOCAP ERROR: no data with name " + pDataName + " found", __FILE__, __FUNCTION__, __LINE__);

	return mData.at(pDataName);
}

void 
MocapSubjectData::addData(const MocapData& pData)  throw (dab::Exception)
{
	const std::string& dataName = pData.name();

	if (mData.find(dataName) != mData.end()) throw dab::Exception("MOCAP ERROR: data with name " + dataName + " already stored", __FILE__, __FUNCTION__, __LINE__);

	mData[dataName] = pData;
}

MocapSubjectData::operator std::string() const
{
	std::stringstream ss;

	ss << "MocapSubjectData:\n";
	ss << "name: " << mName << "\n";
	ss << "skeleton: " << mSkeleton << "\n";

	for (const auto& item : mData)
	{
		ss << "data name: " << item.first << "\n";
		ss << "data: " << item.second << "\n";
	}

	return ss.str();
}

# pragma mark MocapDataset implementation

MocapDataset::MocapDataset()
{}

MocapDataset::~MocapDataset()
{}

void 
MocapDataset::load(const std::string& pFilePath) throw (dab::Exception)
{
	try
	{
		MocapDataSerialize::get().restore(pFilePath, *this);
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("JSON ERROR: failed to load file " + pFilePath, __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

std::vector< std::string > 
MocapDataset::subjectNames() const
{
	std::vector< std::string > _subjectNames;
	for (const auto& item : mSubjects) _subjectNames.push_back(item.first);
	return _subjectNames;
}

MocapSubjectData&
MocapDataset::subjectData(const std::string& pSubjectName) throw (dab::Exception)
{
	if (mSubjects.find(pSubjectName) == mSubjects.end()) throw dab::Exception("MOCAP ERROR: subjec " + pSubjectName + " not found", __FILE__, __FUNCTION__, __LINE__);

	return mSubjects.at(pSubjectName);
}

const MocapSubjectData& 
MocapDataset::subjectData(const std::string& pSubjectName) const throw (dab::Exception)
{
	if (mSubjects.find(pSubjectName) == mSubjects.end()) throw dab::Exception("MOCAP ERROR: subjec " + pSubjectName + " not found", __FILE__, __FUNCTION__, __LINE__);

	return mSubjects.at(pSubjectName);
}

MocapDataset::operator std::string() const
{
	std::stringstream ss;

	ss << "MocapDataset:\n";

	for (const auto& item : mSubjects)
	{
		ss << "data name: " << item.first << "\n";
		ss << "data: " << item.second << "\n";
	}

	return ss.str();
}