/** \file dab_vis_skeleton.cpp
*/

#include "dab_vis_skeleton.h"
#include "dab_vis_shape.h"
#include "dab_math_vec.h"
#include "ofApp.h"
#include <memory>

using namespace dab;

#pragma mark VisSkeleton definition

float VisSkeleton::sJointRadius = 1.0;
float VisSkeleton::sEdgeRadius = 0.5;

VisSkeleton::VisSkeleton(const std::vector< std::vector<int> >& pEdgeConnectivity)
	: mEdgeConnectivity(pEdgeConnectivity)
	, mRootPosition(0.0, -200.0, 0.0)
	, mRootRotation(0.0, 0.0, 0.0)
{
	init();
}

VisSkeleton::~VisSkeleton()
{}

unsigned int 
VisSkeleton::jointCount() const
{
	return mJointShapes.size();
}

void 
VisSkeleton::setJointMaterial(const ofMaterial& pMaterial)
{
	for (auto joint : mJointShapes) joint->setMaterial(pMaterial);
}

void 
VisSkeleton::setEdgeMaterial(const ofMaterial& pMaterial)
{
	for (auto edge : mEdgeShapes) edge->setMaterial(pMaterial);
}

void 
VisSkeleton::setRootPosition(const glm::vec3& pPos)
{
	mRootPosition = pPos;
}

void 
VisSkeleton::setRootRotation(const glm::vec3& pRot)
{
	mRootRotation = pRot;
}

void
VisSkeleton::update(const std::vector<glm::vec3>& pJointPositions) throw (dab::Exception)
{
	math::VectorMath& vecMath = math::VectorMath::get();

	int jointCount = mJointShapes.size();
	int edgeCount = mEdgeShapes.size();

	if (pJointPositions.size() != jointCount) throw dab::Exception("VIS ERROR: number of joint positions doesn't match number of joints", __FILE__, __FUNCTION__, __LINE__);

	for (int jI = 0; jI < jointCount; ++jI)
	{
		mJointShapes[jI]->setTransform(pJointPositions[jI], true);
		//std::cout << "joint " << jI << " pos " << pJointPositions[jI] << "\n";
	}

	for (int jI = 0, eI = 0; jI < jointCount; ++jI)
	{
		int jointEdgeCount = mEdgeConnectivity[jI].size();

		for (int jeI = 0; jeI < jointEdgeCount; ++jeI, ++eI)
		{
			int jI2 = mEdgeConnectivity[jI][jeI];
			const glm::vec3& jP1 = pJointPositions[jI];
			const glm::vec3& jP2 = pJointPositions[jI2];
	
			glm::vec3 edgeVec = jP2 - jP1;
			float edgeLength = glm::length(edgeVec);
			glm::vec3 edgeDir = glm::normalize(edgeVec);
			glm::vec3 refDir(0.0, 1.0, 0.0);

			glm::vec3 edgePos = (jP2 + jP1) * 0.5;
			glm::quat edgeRot = vecMath.makeQuaternion(refDir, edgeDir);

			mEdgeShapes[eI]->setShapeProperties( { sEdgeRadius, edgeLength } );
			mEdgeShapes[eI]->setTransform(edgePos, edgeRot, true);
		}
	}
}

void 
VisSkeleton::draw()
{
	ofSetColor(255, 255, 255);
	//ofPushMatrix();
	//ofRotateX(mRootRotation[0]);
	//ofRotateY(mRootRotation[1]);
	//ofRotateZ(mRootRotation[2]);
	//ofTranslate(mRootPosition);

	int jointCount = mJointShapes.size();
	int edgeCount = mEdgeShapes.size();

	for (int eI = 0; eI < edgeCount; ++eI)
	{
		mEdgeShapes[eI]->drawFaces();
	}

	for (int jI = 0; jI < jointCount; ++jI)
	{
		mJointShapes[jI]->drawFaces();
	}

	//ofPopMatrix();
}

void
VisSkeleton::init()
{
	// add spheres for each joint
	int jointCount = mEdgeConnectivity.size();
	for (int jI = 0; jI < jointCount; ++jI)
	{
		mJointShapes.push_back(std::shared_ptr<VisShape>(new VisShape(VisSphereType, { sJointRadius })));
	}

	// add cylinders for each edge
	for (int jI = 0; jI < jointCount; ++jI)
	{
		std::cout << "joint " << jI << " children: ";

		int childCount = mEdgeConnectivity[jI].size();
		for (int cI = 0; cI < childCount; ++cI)
		{
			std::cout << mEdgeConnectivity[jI][cI] << " ";

			float edgeLength = 100.0; // fixed length, the length will be corrected in the update routine
			mEdgeShapes.push_back(std::shared_ptr<VisShape>(new VisShape(VisCylinderType, { sEdgeRadius, edgeLength })));
		}

		std::cout << "\n";
	}
}

#pragma mark VisSkeletonPlayer implementation

float VisSkeletonPlayer::sFrameRate = 50.0;
bool VisSkeletonPlayer::sLoop = true;

VisSkeletonPlayer::VisSkeletonPlayer(const std::vector< std::vector<int> >& pEdgeConnectivity)
	: mFrameRate(sFrameRate)
	, mLoop(sLoop)
	, mPlay(false)
	, mPlayStartFrame(0)
	, mPlayRate(sFrameRate)
{
	init(pEdgeConnectivity);
}

VisSkeletonPlayer::~VisSkeletonPlayer()
{}

std::shared_ptr<VisSkeleton> 
VisSkeletonPlayer::skeleton()
{
	return mSkeleton;
}

void 
VisSkeletonPlayer::setPositionSequence(const std::vector<float>& pPositionSequence)
{
	int jointCount = mSkeleton->jointCount();
	int frameCount = pPositionSequence.size() / jointCount / 3;

	mPositionSequence = std::vector< std::vector<glm::vec3> >(frameCount, std::vector<glm::vec3>(jointCount));

	for (int fI = 0, dI = 0; fI < frameCount; fI++)
	{
		for (int jI = 0; jI < jointCount; ++jI, dI += 3)
		{
			//std::cout << "fI " << fI << " jI " << jI << "\n";

			mPositionSequence[fI][jI] = glm::vec3(pPositionSequence[dI], pPositionSequence[dI + 1], pPositionSequence[dI + 2]);
		}
	}

	mPlayStopFrame = mPositionSequence.size();
}

void 
VisSkeletonPlayer::setPositionSequence(const std::vector< std::vector<glm::vec3> >& pPositionSequence)
{
	mPositionSequence = pPositionSequence;
	mPlayStopFrame = mPositionSequence.size();
}

void
VisSkeletonPlayer::setPlayRate(float pFrameRate)
{
	mPlayRate = pFrameRate;
}

void 
VisSkeletonPlayer::setPlayStartTime(int pPlayStartTime)
{
	int _playStartFrame = pPlayStartTime * mPlayRate;

	if (_playStartFrame < 0) _playStartFrame = 0;
	if (_playStartFrame > mPlayStopFrame) _playStartFrame = mPlayStopFrame;

	mPlayStartFrame = _playStartFrame;
}

void 
VisSkeletonPlayer::setPlayStopTime(int pPlayStopTime)
{
	int _playStopFrame = pPlayStopTime * mPlayRate;

	if (_playStopFrame < mPlayStartTime) _playStopFrame = mPlayStartTime;
	if (_playStopFrame >= mPositionSequence.size()) _playStopFrame = mPositionSequence.size() - 1;

	mPlayStopFrame = _playStopFrame;
}

void
VisSkeletonPlayer::start()
{
	mPlayStartTime = static_cast<double>(ofGetElapsedTimeMillis()) / 1000.0;
	mPlayFrame = mPlayStartFrame;
	mPlay = true;
}

void
VisSkeletonPlayer::stop()
{
	mPlay = false;
}

const std::vector<glm::vec3>& 
VisSkeletonPlayer::currentPosition() const
{
	return mPositionSequence[mPlayFrame];
}

void 
VisSkeletonPlayer::update()
{
	if (mPlay == false) return;

	double currentTime = static_cast<double>(ofGetElapsedTimeMillis()) / 1000.0;
	double elapsedTime = currentTime - mPlayStartTime;
	int relPlayFrame = mPlayFrame - mPlayStartFrame;
	double nextFrameTime = static_cast<float>(relPlayFrame) / mFrameRate;
	if (elapsedTime > nextFrameTime)
	{
		mPlayFrame++;

		if (mPlayFrame >= mPlayStopFrame)
		{
			mPlayFrame = mPlayStartFrame;
		}

		try
		{
			mSkeleton->update(mPositionSequence[mPlayFrame]);
			mPlay = true;
		}
		catch (dab::Exception& e)
		{
			std::cout << e << "\n";
		}

		// update listeners
		int listenerCount = mListeners.size();
		for (int lI = listenerCount - 1; lI >= 0; --lI)
		{
			mListeners[lI].lock()->notifyUpdate();
		}
	}
}


void 
VisSkeletonPlayer::draw()
{
	mSkeleton->draw();
}

int 
VisSkeletonPlayer::playFrame() const
{
	return mPlayFrame;
}

void 
VisSkeletonPlayer::init(const std::vector< std::vector<int> >& pEdgeConnectivity)
{
	mSkeleton = std::make_shared<VisSkeleton>(pEdgeConnectivity);
}

