/** \file dab_vis_shape.h
*/

#pragma once

#include <iostream>
#include <vector>
#include "of3dPrimitives.h"
#include "ofMesh.h"
#include "ofMaterial.h"
#include "ofVectorMath.h"
#include "dab_exception.h"

namespace dab
{

enum VisShapeType
{
	VisSphereType,
	VisBoxType,
	VisCylinderType,
	VisConeType,
	VisMeshType
};

class VisShape
{
public:
	void test() throw (dab::Exception);

	VisShape(VisShapeType pShapeType, const std::vector<float>& pShapeProperties);
	VisShape(std::shared_ptr<ofMesh> pMesh);
	VisShape(const VisShape& pShape);
	~VisShape();

	VisShape& operator=(const VisShape& pShape);

	static void setSphereResolution(float pSphereResolution);
	static void setCylinderResolution(float pCylinderResolution);
	static void setConeResolution(float pConeResolution);

	bool isSphereShape() const;
	bool isBoxShape() const;
	bool isCylinderShape() const;
	bool isConeShape() const;
	bool isMeshShape() const;

	const std::vector<float>& shapeProperties() const;
	std::shared_ptr<of3dPrimitive> visPrimitive() const;
	ofMaterial& material();

	void setShapeProperties(const std::vector<float>& pShapeProperties) throw (dab::Exception);
	void setMaterial(const ofMaterial& pMaterial);
	void setTransform(const glm::vec3& pPosition, bool pGlobal = true);
	void setTransform(const glm::quat& pOrientation, bool pGlobal = true);
	void setTransform(const glm::vec3& pPosition, const glm::quat& pOrientation, bool pGlobal=true);
	void drawFaces();
	void drawFaces(const ofMaterial& pMaterial);
	void drawWireframe();

protected:
	static float sSphereResolution;
	static float sCylinderResolution; // number of radius segments
	static float sConeResolution; // number of radius segments

	VisShapeType mShapeType;
	std::vector<float> mShapeProperties;
	std::shared_ptr<ofMesh> mMesh;
	std::shared_ptr<of3dPrimitive> mVisPrimitive;
	ofMaterial mMaterial;

	void init() throw (dab::Exception);
	
	void initMeshShape() throw (dab::Exception);
	void initSphereShape() throw (dab::Exception);
	void initBoxShape() throw (dab::Exception);
	void initCylinderShape() throw (dab::Exception);
	void initConeShape() throw (dab::Exception);

	void updateSphereShape() throw (dab::Exception);
	void updateBoxShape() throw (dab::Exception);
	void updateCylinderShape() throw (dab::Exception);
	void updateConeShape() throw (dab::Exception);
};

};