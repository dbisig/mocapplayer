/** \file dab_vis_shape.cpp
*/

#include  "dab_vis_shape.h"

using namespace dab;

float VisShape::sSphereResolution = 8;
float VisShape::sCylinderResolution = 8;
float VisShape::sConeResolution = 8;

void 
VisShape::test() throw (dab::Exception)
{
	throw dab::Exception("my exception", __FILE__, __FUNCTION__, __LINE__);

}

VisShape::VisShape(VisShapeType pShapeType, const std::vector<float>& pShapeProperties)
	: mShapeType(pShapeType)
	, mShapeProperties(pShapeProperties)
	, mMesh(nullptr)
	, mVisPrimitive(nullptr)
{
	try
	{
		init();
	}
	catch(dab::Exception& e)
	{ 
		std::cout << e << "\n";
	}
}

VisShape::VisShape(std::shared_ptr<ofMesh> pMesh)
	: mShapeType(VisMeshType)
	, mMesh(pMesh)
	, mVisPrimitive(nullptr)
{
	try
	{
		init();
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

VisShape::VisShape(const VisShape& pShape)
	: mShapeType(pShape.mShapeType)
	, mShapeProperties(pShape.mShapeProperties)
	, mMaterial(pShape.mMaterial)
	, mMesh(pShape.mMesh)
	, mVisPrimitive(nullptr)
{
	try
	{
		init();
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}
}

VisShape::~VisShape()
{}

VisShape& 
VisShape::operator=(const VisShape& pShape)
{
	mShapeType = pShape.mShapeType;
	mShapeProperties = pShape.mShapeProperties;
	mMaterial = pShape.mMaterial;
	mMesh = pShape.mMesh;
	mVisPrimitive = std::shared_ptr<of3dPrimitive>(nullptr);

	try
	{
		init();
	}
	catch (dab::Exception& e)
	{
		std::cout << e << "\n";
	}

	return *this;
}

void 
VisShape::setSphereResolution(float pSphereResolution)
{
	sSphereResolution = pSphereResolution;
}

void 
VisShape::setCylinderResolution(float pCylinderResolution)
{
	sCylinderResolution = pCylinderResolution;
}

void 
VisShape::setConeResolution(float pConeResolution)
{
	sConeResolution = pConeResolution;
}

bool 
VisShape::isSphereShape() const
{
	return mShapeType == VisSphereType;
}

bool 
VisShape::isBoxShape() const
{
	return mShapeType == VisBoxType;
}

bool 
VisShape::isCylinderShape() const
{
	return mShapeType == VisCylinderType;
}

bool 
VisShape::isConeShape() const
{
	return mShapeType == VisConeType;
}

bool 
VisShape::isMeshShape() const
{
	return mShapeType == VisMeshType;
}

const std::vector<float>& 
VisShape::shapeProperties() const
{
	return mShapeProperties;
}

std::shared_ptr<of3dPrimitive>
VisShape::visPrimitive() const
{
	return mVisPrimitive;
}

ofMaterial& 
VisShape::material()
{
	return mMaterial;
}

void 
VisShape::setShapeProperties(const std::vector<float>& pShapeProperties) throw (dab::Exception)
{
	mShapeProperties = pShapeProperties;

	try
	{
		if (mShapeType == VisSphereType) updateSphereShape();
		else if (mShapeType == VisBoxType) updateBoxShape();
		else if (mShapeType == VisCylinderType) updateCylinderShape();
		else if (mShapeType == VisConeType) updateConeShape();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to set shape properties", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
VisShape::setMaterial(const ofMaterial& pMaterial)
{
	mMaterial = pMaterial;
}

void 
VisShape::setTransform(const glm::vec3& pPosition, bool pGlobal)
{
	if (pGlobal == true)
	{
		mVisPrimitive->setGlobalPosition(pPosition);
	}
	else
	{
		mVisPrimitive->setPosition(pPosition);
	}
}

void 
VisShape::setTransform(const glm::quat& pOrientation, bool pGlobal)
{
	if (pGlobal == true)
	{
		mVisPrimitive->setGlobalOrientation(pOrientation);
	}
	else
	{
		mVisPrimitive->setOrientation(pOrientation);
	}
}

void 
VisShape::setTransform(const glm::vec3& pPosition, const glm::quat& pOrientation, bool pGlobal)
{
	setTransform(pPosition, pGlobal);
	setTransform(pOrientation, pGlobal);
}

void 
VisShape::drawFaces()
{
	mMaterial.begin();
	mVisPrimitive->drawFaces();
	mMaterial.end();
}

void 
VisShape::drawFaces(const ofMaterial& pMaterial)
{
	pMaterial.begin();
	mVisPrimitive->drawFaces();
	pMaterial.end();
}
	
void 
VisShape::drawWireframe()
{
	mMaterial.begin();
	mVisPrimitive->drawWireframe();
	mMaterial.end();
}

void 
VisShape::init() throw (dab::Exception)
{
	try
	{ 
		if(mShapeType == VisSphereType) initSphereShape();
		else if (mShapeType == VisBoxType) initBoxShape();
		else if (mShapeType == VisCylinderType) initCylinderShape();
		else if (mShapeType == VisConeType) initConeShape();
		else if (mShapeType == VisMeshType) initMeshShape();
	}
	catch (dab::Exception& e)
	{
		e += dab::Exception("VIS ERROR: failed to create VisShape", __FILE__, __FUNCTION__, __LINE__);
		throw e;
	}
}

void 
VisShape::initMeshShape() throw (dab::Exception)
{
	if(mMesh == nullptr) throw dab::Exception("VIS ERROR: body mesh not defined", __FILE__, __FUNCTION__, __LINE__);

	mVisPrimitive = std::make_shared<of3dPrimitive>(*mMesh);
}

void 
VisShape::initSphereShape() throw (dab::Exception)
{
	if(mShapeProperties.size() != 1) throw dab::Exception("VIS ERROR: wrong number of shape properties", __FILE__, __FUNCTION__, __LINE__);

	mVisPrimitive = std::make_shared<ofSpherePrimitive>(mShapeProperties[0], sSphereResolution);
}

void 
VisShape::initBoxShape() throw (dab::Exception)
{
	if (mShapeProperties.size() != 3) throw dab::Exception("VIS ERROR: wrong number of box properties", __FILE__, __FUNCTION__, __LINE__);

	mVisPrimitive = std::make_shared<ofBoxPrimitive>(mShapeProperties[0], mShapeProperties[1], mShapeProperties[2], 1, 1, 1);
}

void 
VisShape::initCylinderShape() throw (dab::Exception)
{
	if (mShapeProperties.size() != 2) throw dab::Exception("VIS ERROR: wrong number of cylinder properties", __FILE__, __FUNCTION__, __LINE__);

	mVisPrimitive = std::make_shared<ofCylinderPrimitive>(mShapeProperties[0], mShapeProperties[1], sCylinderResolution, 1);
}

void 
VisShape::initConeShape() throw (dab::Exception)
{
	if (mShapeProperties.size() != 2) throw dab::Exception("VIS ERROR: wrong number of cone properties", __FILE__, __FUNCTION__, __LINE__);

	mVisPrimitive = std::make_shared<ofConePrimitive>(mShapeProperties[0], mShapeProperties[1], sConeResolution, 1, 1);
}

void 
VisShape::updateSphereShape() throw (dab::Exception)
{
	if (mShapeProperties.size() != 1) throw dab::Exception("VIS ERROR: wrong number of shape properties", __FILE__, __FUNCTION__, __LINE__);

	std::shared_ptr<ofSpherePrimitive> sphereShape = std::static_pointer_cast<ofSpherePrimitive>(mVisPrimitive);
	sphereShape->setRadius(mShapeProperties[0]);
}

void 
VisShape::updateBoxShape() throw (dab::Exception)
{
	if (mShapeProperties.size() != 3) throw dab::Exception("VIS ERROR: wrong number of shape properties", __FILE__, __FUNCTION__, __LINE__);

	std::shared_ptr<ofBoxPrimitive> boxShape = std::static_pointer_cast<ofBoxPrimitive>(mVisPrimitive);
	boxShape->setWidth(mShapeProperties[0]);
	boxShape->setHeight(mShapeProperties[1]);
	boxShape->setDepth(mShapeProperties[2]);
}

void 
VisShape::updateCylinderShape() throw (dab::Exception)
{
	if (mShapeProperties.size() != 2) throw dab::Exception("VIS ERROR: wrong number of shape properties", __FILE__, __FUNCTION__, __LINE__);

	std::shared_ptr<ofCylinderPrimitive> cylinderShape = std::static_pointer_cast<ofCylinderPrimitive>(mVisPrimitive);
	cylinderShape->setRadius(mShapeProperties[0]);
	cylinderShape->setHeight(mShapeProperties[1]);
}

void 
VisShape::updateConeShape() throw (dab::Exception)
{
	if (mShapeProperties.size() != 2) throw dab::Exception("VIS ERROR: wrong number of shape properties", __FILE__, __FUNCTION__, __LINE__);

	std::shared_ptr<ofConePrimitive> coneShape = std::static_pointer_cast<ofConePrimitive>(mVisPrimitive);
	coneShape->setRadius(mShapeProperties[0]);
	coneShape->setHeight(mShapeProperties[1]);
}
