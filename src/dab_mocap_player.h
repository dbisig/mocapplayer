#pragma once

#include <vector>
#include "ofVectorMath.h"
#include "ofMaterial.h"
#include "dab_exception.h"
#include "dab_listener.h"

#pragma mark MocapPlayer definition

namespace dab
{

	class MocapPlayer : public dab::UpdateNotifier
	{
	public:
		MocapPlayer();
		~MocapPlayer();

		void setPositionSequence(const std::vector<int64_t>& pPosDim, const std::vector<float>& pPosValues);
		void setPositionSequence(const std::vector< std::vector<glm::vec3> >& pPositionSequence);

		void setRotationSequence(const std::vector<int64_t>& pRotDim, const std::vector<float>& pRotValues);
		void setLocalRotationSequence(const std::vector<int64_t>& pRotDim, const std::vector<float>& pLocalRotValues);

		bool isPlaying() const;
		float playRate() const;
		float playStartTime() const;
		float playStopTime() const;
		bool loop() const;
		float playTime() const;
		
		void setPlayRate(float pPlayRate);
		void setPlayStartTime(float pPlayStartTime);
		void setPlayStopTime(float pPlayStopTime);
		void setLoop(bool pLoop);

		void start();
		void stop();
		void update();

		int playFrame() const;
		const std::vector<glm::vec3>& currentPosition() const;
		const std::vector<glm::quat>& currentRotation() const;
		const std::vector<glm::quat>& currentLocalRotation() const;

	protected:
		static float sFrameRate;
		static bool sLoop;

		std::vector< std::vector<glm::vec3> > mPositionSequence;
		std::vector< std::vector<glm::quat> > mRotationSequence;
		std::vector< std::vector<glm::quat> > mLocalRotationSequence;

		float mFrameRate;
		bool mPlay;
		int mPlayFrame;
		int mPlayStartFrame;
		int mPlayStopFrame;
		double mStartTime;

		float mPlayRate;
		float mPlayStartTime;
		float mPlayStopTime;
		bool mLoop;
	};

};