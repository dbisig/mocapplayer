# Motion Capture Player

Daniel Bisig - Coventry University, UK - [ad5041@coventry.ac.uk](ad5041@coventry.ac.uk)

## Abstract

This software is a simple tool for playing motion capture data and simultaneously sending this data via the open sound control (OSC) protocol to a client. This data is stored in a custom file format that can be created using simple python scripts. The player is written in C++ using the OpenFrameworks creative coding environment. 

## Usage
![Player](./content/player_screenshot.jpg "Player")

**Conversion of Skeleton Motion Capture Data**

Conversion of skeleton motion capture data is from the Biovision (.bvh) format into the custom json format required by the player.

python bvhconv.py --input ../data/example_mocap.bvh --output ../data/example_mocap_skel.json

**Compiling the Motion Capture Player**

Currently, the player requires OpenFrameworks version 0.11.0 to compile. Furthermore, it has the following dependencies:

- [ofxDabBase](https://bitbucket.org/dbisig/ofxdabbase_011/src/master/ "Bitbucket")
- [ofxDabMath](https://bitbucket.org/dbisig/ofxdabmath_011/src/master/ "Bitbucket")
- [ofxDabOsc](https://bitbucket.org/dbisig/ofxdabosc_011/src/master/ "Bitbucket")
- [ofxImGui](https://github.com/jvcleave/ofxImGui "Github")
- [ofxJSON](https://github.com/jeffcrouse/ofxJSON "Github")

**Configuration of Motion Capture Player**

To configure the player, the file ./bin/data/config.json needs to be edited. Configuration involves specifying the file the player should read motion capture data from and the address and port the player sends the OSC messages to.

    "mocapFileName": "data/MUR_AccumulationMovementOnPlace_01_mb_proc_rh.json",
    "oscSendAddress": "127.0.0.1",
    "oscSendPort": 9003

The OSC message content is as follows:

For skeleton data: /mocap/skel



## Resources

- E2-Create [Project Page](https://wp.coventry.domains/e2create/ "Project Page")